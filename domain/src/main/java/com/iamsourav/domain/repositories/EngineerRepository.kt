package com.iamsourav.domain.repositories

import com.iamsourav.domain.entities.Engineer
import io.reactivex.Single
import java.util.*

/**
 * Created by Sourav Palit on 2019-11-11
 */
interface EngineerRepository {
    fun getEngineerList(): Single<List<Engineer>>
}
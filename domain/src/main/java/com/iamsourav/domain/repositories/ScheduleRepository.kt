package com.iamsourav.domain.repositories

import com.iamsourav.domain.entities.Engineer
import com.iamsourav.domain.entities.Schedule
import io.reactivex.Single

/**
 * Created by Sourav Palit on 2019-11-11
 */
interface ScheduleRepository {
    fun getSchedules(engineerList: List<Engineer>) : Single<List<Schedule>>
}
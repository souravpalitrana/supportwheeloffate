package com.iamsourav.domain.repositories.criteria

import com.iamsourav.domain.entities.Engineer

interface FilterCriteria {

    fun meetCriteria(date: String, engineerList: MutableList<Engineer>) : MutableList<Engineer>
}
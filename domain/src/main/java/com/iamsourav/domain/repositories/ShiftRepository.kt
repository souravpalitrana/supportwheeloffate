package com.iamsourav.domain.repositories

import com.iamsourav.domain.entities.Shift

/**
 * Created by Sourav Palit on 2019-11-11
 */
interface ShiftRepository {
    fun getLastShiftTimeOfEngineer(engineerId: Int) : String
    fun getTotalShiftOfEngineer(engineerId: Int) : Int
    fun getShiftCountOfShiftOfEngineer(engineerId: Int, shiftType: Int) : Int
    fun addShift(shift: Shift)
    fun reset()
}
package com.iamsourav.domain.exceptions

/**
 * Created by Sourav Palit on 2019-11-11
 */
abstract class WheelError : Throwable() {
    abstract fun getWheelException(): Throwable?
    abstract fun getErrorMessage(): String
    abstract fun getLocalizedMessage(): String
}
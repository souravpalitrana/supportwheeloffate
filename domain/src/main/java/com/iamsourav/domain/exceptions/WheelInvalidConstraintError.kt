package com.iamsourav.domain.exceptions

import com.sun.xml.internal.bind.v2.TODO

/**
 * Created by Sourav Palit on 2019-11-11
 */
class WheelInvalidConstraintError(val exception: Throwable?) : WheelError() {

    val DEFAULT_ERROR_MESSAGE = "Could not generate schedule. Please check your constraints"

    override fun getWheelException(): Throwable? = exception

    override fun getErrorMessage() = exception?.message?:DEFAULT_ERROR_MESSAGE

    override fun getLocalizedMessage() = DEFAULT_ERROR_MESSAGE
}
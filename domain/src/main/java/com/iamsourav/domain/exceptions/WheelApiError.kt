package com.iamsourav.domain.exceptions

import com.sun.xml.internal.bind.v2.TODO

/**
 * Created by Sourav Palit on 2019-11-11
 */
class WheelApiError(val exception: Throwable?) : WheelError() {

    val DEFAULT_ERROR_MESSAGE = "Something went wrong. Try again later."

    override fun getWheelException(): Throwable? = exception

    override fun getErrorMessage() = exception?.message?:DEFAULT_ERROR_MESSAGE

    override fun getLocalizedMessage() = DEFAULT_ERROR_MESSAGE
}
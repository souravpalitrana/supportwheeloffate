package com.iamsourav.domain.observer

import io.reactivex.observers.DisposableSingleObserver

/**
 * Created by Sourav Palit on 2019-11-11
 * Default [DisposableObserver] base class to be used whenever you want default error handling.
 */
abstract class DefaultObserver<T> : DisposableSingleObserver<T>() {
    abstract override fun onSuccess(t: T)

    abstract override fun onError(exception: Throwable)
}

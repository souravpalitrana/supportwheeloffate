package com.iamsourav.domain.base

/**
 * Created by Sourav Palit on 2019-11-12
 */
//Pass
data class ScheduleConfig(val numberOfEngineerPerShift: Int = 1,
                          val totalWeeks: Int = 2,
                          val workingDaysPerWeek: Int = 5,
                          val totalShiftsPerDay: Int = 2,
                          val numberOfShiftsEngineerCanDoPerDay: Int = 1,
                          val minimumDayBetweenNextShift: Int = 1,
                          val maxShiftByAnEnginner: Int = 2)
//Fail
/*data class ScheduleConfig(val numberOfEngineerPerShift: Int = 1,
                          val totalWeeks: Int = 2,
                          val workingDaysPerWeek: Int = 5,
                          val totalShiftsPerDay: Int = 2,
                          val numberOfShiftsEngineerCanDoPerDay: Int = 1,
                          val minimumDayBetweenNextShift: Int = 1,
                          val maxShiftByAnEnginner: Int = 1)*/

// Rule 2 : Number of Engineer Per Shift 2, Number of Shift 2
// Pass
/*data class ScheduleConfig(val numberOfEngineerPerShift: Int = 2,
                          val totalWeeks: Int = 1,
                          val workingDaysPerWeek: Int = 5,
                          val totalShiftsPerDay: Int = 2,
                          val numberOfShiftsEngineerCanDoPerDay: Int = 1,
                          val minimumDayBetweenNextShift: Int = 2,
                          val maxShiftByAnEnginner: Int = 2)*/
/*//Fail
data class ScheduleConfig(val numberOfEngineerPerShift: Int = 2,
                          val totalWeeks: Int = 1,
                          val workingDaysPerWeek: Int = 5,
                          val totalShiftsPerDay: Int = 2,
                          val numberOfShiftsEngineerCanDoPerDay: Int = 1,
                          val minimumDayBetweenNextShift: Int = 1,
                          val maxShiftByAnEnginner: Int = 1)*/

// Rule 3 Number of Engineer per shift 1, Number of Shift 3
//Pass
/*data class ScheduleConfig(val numberOfEngineerPerShift: Int = 1,
                          val totalWeeks: Int = 1,
                          val workingDaysPerWeek: Int = 5,
                          val totalShiftsPerDay: Int = 3,
                          val numberOfShiftsEngineerCanDoPerDay: Int = 1,
                          val minimumDayBetweenNextShift: Int = 3,
                          val maxShiftByAnEnginner: Int = 2)*/

//Fail
/*data class ScheduleConfig(val numberOfEngineerPerShift: Int = 1,
                          val totalWeeks: Int = 1,
                          val workingDaysPerWeek: Int = 5,
                          val totalShiftsPerDay: Int = 3,
                          val numberOfShiftsEngineerCanDoPerDay: Int = 1,
                          val minimumDayBetweenNextShift: Int = 3,
                          val maxShiftByAnEnginner: Int = 1)*/
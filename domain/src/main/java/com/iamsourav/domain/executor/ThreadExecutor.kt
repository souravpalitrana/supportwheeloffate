package com.appsdreamers.domain.executor

import java.util.concurrent.Executor

/**
 * Created by Sourav Palit on 2019-11-11
 * Executor implementation can be based on different frameworks or techniques of asynchronous
 * execution, but every implementation will execute the
 */
public interface ThreadExecutor : Executor

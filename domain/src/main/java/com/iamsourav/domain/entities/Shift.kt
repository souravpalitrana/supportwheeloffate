package com.iamsourav.domain.entities

/**
 * Created by Sourav Palit on 2019-11-11
 * A shift may have a type and multiple engineer can work on a shift.
 */
data class Shift(var type: Int,
                 var date: String,
                 var engineers:List<Engineer>)
package com.iamsourav.domain.entities

/**
 * Created by Sourav Palit on 2019-11-11
 * A schedule may have multiple shift
 */
data class Schedule(var date: String,
                    var shiftList:List<Shift>)
package com.iamsourav.domain.usecases

import com.appsdreamers.domain.executor.PostExecutionThread
import com.appsdreamers.domain.executor.ThreadExecutor
import com.iamsourav.domain.entities.Engineer
import com.iamsourav.domain.entities.Schedule
import com.iamsourav.domain.repositories.EngineerRepository
import com.iamsourav.domain.repositories.ScheduleRepository
import io.reactivex.Single
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

/**
 * Created by Sourav Palit on 2019-11-12
 */
class GetScheduleListUseCase @Inject constructor(threadExecutor: ThreadExecutor,
                                                 postExecutionThread: PostExecutionThread,
                                                 val scheduleRepository: ScheduleRepository
) : UseCase<List<Schedule>>(threadExecutor, postExecutionThread) {

    lateinit var engineerList: List<Engineer>

    public fun setParam(engineers: List<Engineer>) {
        this.engineerList = engineers
    }

    override fun buildUseCaseObservable(): Single<List<Schedule>> {
        return scheduleRepository.getSchedules(engineerList)
    }
}
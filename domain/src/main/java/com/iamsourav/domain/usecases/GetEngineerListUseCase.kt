package com.iamsourav.domain.usecases

import com.appsdreamers.domain.executor.PostExecutionThread
import com.appsdreamers.domain.executor.ThreadExecutor
import com.iamsourav.domain.entities.Engineer
import com.iamsourav.domain.repositories.EngineerRepository
import io.reactivex.Single
import java.util.*
import javax.inject.Inject

/**
 * Created by Sourav Palit on 2019-11-11
 */
class GetEngineerListUseCase @Inject constructor(threadExecutor: ThreadExecutor,
                              postExecutionThread: PostExecutionThread,
                              val engineerRepository: EngineerRepository
) : UseCase<List<Engineer>>(threadExecutor, postExecutionThread) {

    override fun buildUseCaseObservable(): Single<List<Engineer>> {
        return engineerRepository.getEngineerList()
    }
}
package com.iamsourav.supportwheeloffate.ui.schedulelist.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.iamsourav.domain.entities.Schedule
import com.iamsourav.supportwheeloffate.ui.common.DividerItemDecoration
import kotlinx.android.synthetic.main.item_schedule_list_date.view.*

/**
 * Created by Sourav Palit on 2019-11-13
 */
class ScheduleListAdapter(val context : Context,
                          var scheduleList : List<Schedule>
) : RecyclerView.Adapter<ScheduleListAdapter.ScheduleItemViewHolder>() {

    val layoutInflater : LayoutInflater

    init {
        layoutInflater = LayoutInflater.from(context)
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ScheduleItemViewHolder {
        val view = layoutInflater.inflate(com.iamsourav.supportwheeloffate.R.layout.item_schedule_list_date, parent, false)
        return ScheduleItemViewHolder(view)
    }

    override fun getItemCount() = scheduleList.size

    override fun onBindViewHolder(holder: ScheduleItemViewHolder, position: Int) {
        holder.bindData(scheduleList.get(position), position)
    }

    class ScheduleItemViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {

        fun bindData(schedule: Schedule, position: Int) {

            if (position % 2 == 0) {
                itemView.rootView
                    .setBackgroundColor(ContextCompat.getColor(itemView.context, com.iamsourav.supportwheeloffate.R.color.colorEven))
            } else {
                itemView.rootView
                    .setBackgroundColor(ContextCompat.getColor(itemView.context, com.iamsourav.supportwheeloffate.R.color.colorOdd))
            }

            itemView.tvDate.text = "Day ${schedule.date}"

            val adapter = ShiftListAdapter(itemView.context, schedule.shiftList)

            itemView.rvShiftList.layoutManager = LinearLayoutManager(itemView.context)
            itemView.rvShiftList.adapter = adapter
            itemView.rvShiftList.addItemDecoration(DividerItemDecoration(itemView.context))
        }
    }
}
package com.iamsourav.supportwheeloffate.ui.engineerlist.view

import android.app.Activity
import android.os.Bundle
import android.view.*
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.navOptions
import androidx.recyclerview.widget.LinearLayoutManager
import com.iamsourav.supportwheeloffate.ui.engineerlist.model.Engineer
import com.iamsourav.supportwheeloffate.R
import com.iamsourav.supportwheeloffate.base.SupportWheelOfFateApplication
import com.iamsourav.supportwheeloffate.ui.base.KEY_EXTRA
import com.iamsourav.supportwheeloffate.ui.common.view.StatusView
import com.iamsourav.supportwheeloffate.ui.engineerlist.EngineerListContract
import kotlinx.android.synthetic.main.fragment_engineer_list.*
import javax.inject.Inject

/**
 * Created by Sourav Palit on 2019-11-11
 * Fragment used to show the list of all engineers
 */
class EngineerListFragment : Fragment(), EngineerListContract.EngineerListView {

    @Inject
    lateinit var mPresenter : EngineerListContract.EngineerListPresenter

    override fun getContext() = this.activity!!.applicationContext

    override fun getHostActivity() = this.activity as Activity

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_engineer_list, container, false)
        initViews(view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setup()
    }

    override fun onDestroyView() {
        mPresenter.dettachView(this)
        super.onDestroyView()
    }

    override fun showLoading() {
        viewStatus.showLoadingState()
        viewStatus.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        viewStatus.visibility = View.GONE
    }

    override fun showError(errorMessage: String) {
        viewStatus.visibility = View.VISIBLE
        viewStatus.showErrorState(errorMessage)
    }

    override fun onDataLoaded(engineerList: List<Engineer>) {
        val listAdapter = EngineerListAdapter(context, engineerList)
        rvEngineerList.layoutManager = LinearLayoutManager(activity)
        rvEngineerList.adapter = listAdapter
        rvEngineerList.hasFixedSize()
        rvEngineerList.visibility = View.VISIBLE
        btnGenerateSchedule.visibility = View.VISIBLE
    }

    override fun navigateToScheduleList(engineerList: List<Engineer>) {
        val options = navOptions {
            anim {
                enter = R.anim.slide_in_right
                exit = R.anim.slide_out_left
                popEnter = R.anim.slide_in_left
                popExit = R.anim.slide_out_right
            }
        }

        val args = Bundle()
        args.putParcelableArrayList(KEY_EXTRA, engineerList as ArrayList<Engineer>)
        findNavController().navigate(R.id.action_generatr_schedule, args, options)
    }

    private fun initViews(view : View) {
        view.findViewById<Button>(R.id.btnGenerateSchedule).setOnClickListener {
                mPresenter.onGenerateScheduleClicked()
        }

        view.findViewById<StatusView>(R.id.viewStatus)
            .setStatusViewListener(object : StatusView.StatusViewListener {
            override fun onBtnRetryClicked() {
                mPresenter.onTryAgainClicked()
            }
        })
    }

    private fun setup() {
        SupportWheelOfFateApplication.instance.appComponent.inject(this)
        mPresenter.attachView(this)
    }

}

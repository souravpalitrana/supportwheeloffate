package com.iamsourav.supportwheeloffate.ui.common.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.RelativeLayout
import com.iamsourav.supportwheeloffate.R
import kotlinx.android.synthetic.main.view_status.view.*

/**
 * Created by Sourav Palit on 2019-11-11
 * Composite view to handle loading and error state
 */
class StatusView : RelativeLayout {

    var listenner: StatusViewListener? = null

    constructor(context: Context,
                attrs : AttributeSet) : super(context, attrs) {
        init()
    }

    constructor(
        context : Context,
        attrs : AttributeSet,
        defStyleAttr : Int) : super(context, attrs, defStyleAttr) {

        init()
    }

    private fun init() {
        View.inflate(context, R.layout.view_status, this);
        btnTryAgain.setOnClickListener {
            listenner?.onBtnRetryClicked()
        }
        showLoadingState()
    }

    fun setStatusViewListener(statusViewListener: StatusViewListener) {
        this.listenner = statusViewListener
    }

    fun showLoadingState() {
        pbLoader.visibility = View.VISIBLE
        ivError.visibility = View.GONE
        btnTryAgain.visibility = View.INVISIBLE
        btnTryAgain.isEnabled = false
        tvMessage.text = this.context.getString(R.string.txt_loading)
    }

    fun showErrorState(errorMessage: String) {
        pbLoader.visibility = View.GONE
        ivError.visibility = View.VISIBLE
        btnTryAgain.visibility = View.VISIBLE
        btnTryAgain.isEnabled = true
        tvMessage.text = errorMessage
    }

    interface StatusViewListener {
        fun onBtnRetryClicked()
    }
}
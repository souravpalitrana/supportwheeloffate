package com.iamsourav.supportwheeloffate.ui.engineerlist.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by Sourav Palit on 2019-11-12
 */
@Parcelize
data class Engineer(var id: Int,
                    var name: String) : Parcelable
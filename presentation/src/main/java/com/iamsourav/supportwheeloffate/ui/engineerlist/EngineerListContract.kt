package com.iamsourav.supportwheeloffate.ui.engineerlist

import com.iamsourav.supportwheeloffate.ui.base.BasePresenter
import com.iamsourav.supportwheeloffate.ui.base.BaseView
import com.iamsourav.supportwheeloffate.ui.engineerlist.model.Engineer

/**
 * Created by Sourav Palit on 2019-11-11
 */
interface EngineerListContract {

    interface EngineerListView : BaseView {
        fun showLoading()
        fun hideLoading()
        fun showError(errorMessage: String)
        fun onDataLoaded(engineerList: List<Engineer>)
        fun navigateToScheduleList(engineerList: List<Engineer>)
    }

    interface EngineerListPresenter : BasePresenter<EngineerListView> {
        fun onGenerateScheduleClicked()
        fun onTryAgainClicked()
    }
}
package com.iamsourav.supportwheeloffate.ui.base

import android.app.Activity
import android.content.Context

public interface BaseView {
    fun getContext() : Context
    fun getHostActivity() : Activity
}
package com.iamsourav.supportwheeloffate.ui.base

public interface BasePresenter<T> {
    fun attachView(view : T)
    fun dettachView(view : T)
    fun onResume()
    fun onPause()
}
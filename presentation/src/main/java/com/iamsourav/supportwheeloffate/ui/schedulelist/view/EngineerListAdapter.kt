package com.iamsourav.supportwheeloffate.ui.schedulelist.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.iamsourav.supportwheeloffate.ui.engineerlist.model.Engineer
import com.iamsourav.supportwheeloffate.R
import kotlinx.android.synthetic.main.item_enginner_list.view.*

/**
 * Created by Sourav Palit on 2019-11-11
 */
class EngineerListAdapter(val context : Context,
                          var engineerList : List<Engineer>
) : RecyclerView.Adapter<EngineerListAdapter.EngineerItemViewHolder>() {

    val layoutInflater : LayoutInflater

    init {
        layoutInflater = LayoutInflater.from(context)
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): EngineerItemViewHolder {
        val view = layoutInflater.inflate(R.layout.item_schedule_list_engineer, parent, false)
        return EngineerItemViewHolder(view)
    }

    override fun getItemCount() = engineerList.size

    override fun onBindViewHolder(holder: EngineerItemViewHolder, position: Int) {
        holder.bindData(engineerList.get(position))
    }

    class EngineerItemViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {

        fun bindData(engineer: Engineer) {
            itemView.tvEnginerName.text = engineer.name
        }
    }
}
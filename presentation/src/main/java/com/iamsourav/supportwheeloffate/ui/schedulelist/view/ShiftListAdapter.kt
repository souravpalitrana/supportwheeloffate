package com.iamsourav.supportwheeloffate.ui.schedulelist.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.iamsourav.domain.entities.Shift
import com.iamsourav.supportwheeloffate.R
import com.iamsourav.supportwheeloffate.mapper.mapDomainToPresentationEngineer
import com.iamsourav.supportwheeloffate.ui.common.DividerItemDecoration
import kotlinx.android.synthetic.main.fragment_engineer_list.view.rvEngineerList
import kotlinx.android.synthetic.main.item_schedule_list_shift.view.*

/**
 * Created by Sourav Palit on 2019-11-13
 */
class ShiftListAdapter(val context : Context,
                       var shiftList : List<Shift>
) : RecyclerView.Adapter<ShiftListAdapter.ShiftItemViewHolder>() {

    val layoutInflater : LayoutInflater

    init {
        layoutInflater = LayoutInflater.from(context)
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ShiftItemViewHolder {
        val view = layoutInflater.inflate(R.layout.item_schedule_list_shift, parent, false)
        return ShiftItemViewHolder(view)
    }

    override fun getItemCount() = shiftList.size

    override fun onBindViewHolder(holder: ShiftItemViewHolder, position: Int) {
        holder.bindData(shiftList.get(position))
    }

    class ShiftItemViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {

        fun bindData(shift: Shift) {

            itemView.tvShift.text = "Shift ${shift.type}"

            val adapter = EngineerListAdapter(itemView.context, mapDomainToPresentationEngineer(shift.engineers))

            itemView.rvEngineerList.layoutManager = LinearLayoutManager(itemView.context)
            itemView.rvEngineerList.adapter = adapter
            itemView.rvEngineerList.addItemDecoration(DividerItemDecoration(itemView.context))
        }
    }
}
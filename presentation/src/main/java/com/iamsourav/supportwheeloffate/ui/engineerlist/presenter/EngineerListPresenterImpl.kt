package com.iamsourav.supportwheeloffate.ui.engineerlist.presenter


import com.iamsourav.domain.entities.Engineer as DomainEngineer
import com.iamsourav.domain.observer.DefaultObserver
import com.iamsourav.domain.usecases.GetEngineerListUseCase
import com.iamsourav.supportwheeloffate.R
import com.iamsourav.supportwheeloffate.base.isNetworkConnected
import com.iamsourav.supportwheeloffate.mapper.mapDomainToPresentationEngineer
import com.iamsourav.supportwheeloffate.ui.engineerlist.EngineerListContract
import com.iamsourav.supportwheeloffate.ui.engineerlist.model.Engineer

/**
 * Created by Sourav Palit on 2019-11-11
 */
class EngineerListPresenterImpl(
    val getEngineerListUseCase : GetEngineerListUseCase
) : EngineerListContract.EngineerListPresenter {

    var mView : EngineerListContract.EngineerListView? = null
    var engineerList = ArrayList<Engineer>()

    override fun attachView(view: EngineerListContract.EngineerListView) {
        mView = view
        handleData()
    }

    override fun dettachView(view: EngineerListContract.EngineerListView) {
        mView = null
    }

    override fun onResume() { }

    override fun onPause() { }

    override fun onGenerateScheduleClicked() {
        mView?.navigateToScheduleList(engineerList)
    }

    override fun onTryAgainClicked() {
        handleData()
    }

    private fun handleData() {
        if (isNetworkConnected(mView?.getContext())) {
            loadEngineerList()
        } else {
            mView?.showError(mView!!.getContext().getString(R.string.txt_no_internet))
        }
    }

    private fun loadEngineerList() {
        mView?.showLoading()
        getEngineerListUseCase.execute(object : DefaultObserver<List<DomainEngineer>>(){
            override fun onSuccess(domainEngineerList: List<DomainEngineer>) {
                engineerList.addAll(mapDomainToPresentationEngineer(domainEngineerList))
                mView?.onDataLoaded(engineerList)
                mView?.hideLoading()
            }

            override fun onError(exception: Throwable) {
                mView?.showError(exception.localizedMessage)
            }
        })
    }
}
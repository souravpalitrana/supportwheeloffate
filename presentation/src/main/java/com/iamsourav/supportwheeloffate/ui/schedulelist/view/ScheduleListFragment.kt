package com.iamsourav.supportwheeloffate.ui.schedulelist.view

import android.app.Activity
import android.os.Bundle
import android.os.Handler
import android.view.*
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.iamsourav.domain.entities.Schedule
import com.iamsourav.supportwheeloffate.R
import com.iamsourav.supportwheeloffate.base.SupportWheelOfFateApplication
import com.iamsourav.supportwheeloffate.ui.base.KEY_EXTRA
import com.iamsourav.supportwheeloffate.ui.common.DividerItemDecoration
import com.iamsourav.supportwheeloffate.ui.engineerlist.model.Engineer
import com.iamsourav.supportwheeloffate.ui.schedulelist.ScheduleListContract
import kotlinx.android.synthetic.main.fragment_schedule_list.*
import kotlinx.android.synthetic.main.fragment_schedule_list.viewStatus
import javax.inject.Inject

/**
 * Created by Sourav Palit on 2019-11-11
 * Fragment used to show the list of support schedule of the engineers
 */
class ScheduleListFragment : Fragment(), ScheduleListContract.ScheduleListView {

    @Inject
    lateinit var mPresenter : ScheduleListContract.ScheduleListPresenter

    override fun getContext() = this.activity!!.applicationContext

    override fun getHostActivity() = this.activity as Activity

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_schedule_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setup()
    }

    override fun onDestroyView() {
        mPresenter.dettachView(this)
        super.onDestroyView()
    }
    override fun showLoading() {
        viewStatus.showLoadingState()
        viewStatus.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        Handler().postDelayed({
            viewStatus.visibility = View.GONE
        }, 700)
    }

    override fun showError(errorMessage: String) {
        viewStatus.visibility = View.VISIBLE
        viewStatus.showErrorState(errorMessage)
    }

    override fun getEngineerList() : List<Engineer>? {

        arguments?.let {
            return arguments!!.getParcelableArrayList<Engineer>(KEY_EXTRA)!!
        }

        return null
    }

    override fun onScheduleLoaded(scheduleList: List<Schedule>) {
        val listAdapter = ScheduleListAdapter(context, scheduleList)
        rvScheduleList.layoutManager = LinearLayoutManager(activity)
        rvScheduleList.adapter = listAdapter
        rvScheduleList.addItemDecoration(DividerItemDecoration(context))
        rvScheduleList.hasFixedSize()
        rvScheduleList.visibility = View.VISIBLE
    }

    private fun setup() {
        SupportWheelOfFateApplication.instance.appComponent.inject(this)
        mPresenter.attachView(this)
    }
}

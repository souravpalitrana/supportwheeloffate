package com.iamsourav.supportwheeloffate.ui.schedulelist.presenter

import com.iamsourav.domain.entities.Schedule
import com.iamsourav.domain.observer.DefaultObserver
import com.iamsourav.domain.usecases.GetScheduleListUseCase
import com.iamsourav.supportwheeloffate.mapper.mapPresentationToDomainEngineer
import com.iamsourav.supportwheeloffate.ui.engineerlist.model.Engineer
import com.iamsourav.supportwheeloffate.ui.schedulelist.ScheduleListContract

/**
 * Created by Sourav Palit on 2019-11-11
 */
class ScheduleListPresenterImpl(
    val getScheduleListUseCase: GetScheduleListUseCase
) : ScheduleListContract.ScheduleListPresenter {

    var mView : ScheduleListContract.ScheduleListView? = null

    override fun attachView(view: ScheduleListContract.ScheduleListView) {
        mView = view

        mView?.getEngineerList()?.let {
            loadScheduleList(it)
        }
    }

    override fun dettachView(view: ScheduleListContract.ScheduleListView) {
        mView = null
    }

    override fun onResume() { }

    override fun onPause() { }

    private fun loadScheduleList(engineerList: List<Engineer>) {

        mView?.showLoading()

        getScheduleListUseCase.setParam(mapPresentationToDomainEngineer(engineerList))

        getScheduleListUseCase.execute(object : DefaultObserver<List<Schedule>>(){
            override fun onSuccess(domainEngineerList: List<Schedule>) {
                mView?.hideLoading()
                mView?.onScheduleLoaded(domainEngineerList)
            }

            override fun onError(exception: Throwable) {
                mView?.showError(exception.localizedMessage)
            }
        })
    }
}
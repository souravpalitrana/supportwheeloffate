package com.iamsourav.supportwheeloffate.ui.schedulelist

import com.iamsourav.domain.entities.Schedule
import com.iamsourav.supportwheeloffate.ui.base.BasePresenter
import com.iamsourav.supportwheeloffate.ui.base.BaseView
import com.iamsourav.supportwheeloffate.ui.engineerlist.model.Engineer

/**
 * Created by Sourav Palit on 2019-11-11
 */
interface ScheduleListContract {

    interface ScheduleListView : BaseView {
        fun showLoading()
        fun hideLoading()
        fun showError(errorMessage: String)
        fun getEngineerList() : List<Engineer>?
        fun onScheduleLoaded(scheduleList: List<Schedule>)
    }

    interface ScheduleListPresenter : BasePresenter<ScheduleListView> {

    }
}
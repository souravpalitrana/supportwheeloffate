package com.iamsourav.supportwheeloffate.di.scopes

import javax.inject.Scope
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy

/**
 * Created by Sourav Palit on 2019-11-11
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
annotation class PerActivity


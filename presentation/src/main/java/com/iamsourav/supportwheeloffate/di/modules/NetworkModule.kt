package com.iamsourav.supportwheeloffate.di.modules


import com.iamsourav.data.repositories.datasource.apiservices.WheelApiService
import com.iamsourav.supportwheeloffate.BuildConfig
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Sourav Palit on 2019-11-11
 */
@Module
class NetworkModule {

    @Provides
    @Singleton
    internal fun provideWheelApiService(retrofit : Retrofit): WheelApiService {
        return retrofit.create(WheelApiService::class.java)
    }

    @Provides
    @Singleton
    internal fun providesRetrofit(client: OkHttpClient): Retrofit {

        return Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(client)
            .build()
    }

    @Provides
    @Singleton
    internal fun okHttpClient(
        loggingInterceptor: HttpLoggingInterceptor,
        headerInterceptor: Interceptor
    ): OkHttpClient {
        val builder = OkHttpClient.Builder()

        if (BuildConfig.DEBUG) {
            builder.addInterceptor(loggingInterceptor)
        }

        builder.readTimeout(60, TimeUnit.SECONDS)
        builder.connectTimeout(60, TimeUnit.SECONDS)
        builder.writeTimeout(60, TimeUnit.SECONDS)

        builder.addInterceptor(headerInterceptor)

        return builder.build()
    }


    @Provides
    @Singleton
    internal fun getHeaderInterceptor(): Interceptor {
        return Interceptor { chain ->
            val original = chain.request()
            val builder = original.newBuilder()
                .addHeader("App-Agent", "android/${BuildConfig.VERSION_CODE}")
            chain.proceed(builder.build())
        }
    }

    @Provides
    @Singleton
    internal fun providesLoggingInterceptor(): HttpLoggingInterceptor {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return interceptor
    }

}
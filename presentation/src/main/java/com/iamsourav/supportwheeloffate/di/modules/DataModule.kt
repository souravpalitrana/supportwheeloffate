package com.iamsourav.supportwheeloffate.di.modules

import android.os.Build.VERSION_CODES.M
import android.provider.SyncStateContract
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.iamsourav.data.repositories.EngineerListRepositoryImpl
import com.iamsourav.data.repositories.ScheduleRepositoryImpl
import com.iamsourav.data.repositories.ShiftRepositoryImpl
import com.iamsourav.data.repositories.datasource.apiservices.WheelApiService
import com.iamsourav.data.repositories.datasource.criteria.MaxOneShiftsInTwoConsecutiveDaysFilter
import com.iamsourav.data.repositories.datasource.criteria.MaxShiftFilter
import com.iamsourav.data.repositories.datasource.criteria.MinimumShiftFilter
import com.iamsourav.domain.base.ScheduleConfig
import com.iamsourav.domain.repositories.EngineerRepository
import com.iamsourav.domain.repositories.ScheduleRepository
import com.iamsourav.domain.repositories.ShiftRepository
import com.iamsourav.domain.repositories.criteria.FilterCriteria
import com.iamsourav.supportwheeloffate.BuildConfig
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import java.io.IOException
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Sourav Palit on 2019-11-11
 */
@Module
class DataModule {

    @Provides
    @Singleton
    internal fun providesEngineerRepository(apiService: WheelApiService) : EngineerRepository {
        return EngineerListRepositoryImpl(apiService)
    }

    @Provides
    @Singleton
    internal fun providesShiftRepository() : ShiftRepository {
        return ShiftRepositoryImpl()
    }

    @Provides
    @Singleton
    internal fun providesScheduleConfig() : ScheduleConfig {
        return ScheduleConfig()
    }

    @Provides
    @Singleton
    internal fun providesFilters(shiftRepository: ShiftRepository,
                                 scheduleConfig: ScheduleConfig) : ArrayList<FilterCriteria> {

        val filters = ArrayList<FilterCriteria>()
        filters.add(MaxShiftFilter(shiftRepository, scheduleConfig))
        filters.add(MinimumShiftFilter(shiftRepository, scheduleConfig))
        filters.add(MaxOneShiftsInTwoConsecutiveDaysFilter(shiftRepository, scheduleConfig))

        return filters
    }

    @Provides
    @Singleton
    internal fun providesScheduleRepository(scheduleConfig: ScheduleConfig,
                                            shiftRepository: ShiftRepository,
                                            filters: ArrayList<FilterCriteria>
    ) : ScheduleRepository {

        return ScheduleRepositoryImpl(scheduleConfig, shiftRepository, filters)
    }
}
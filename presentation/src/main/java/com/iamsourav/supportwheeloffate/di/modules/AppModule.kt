package com.iamsourav.supportwheeloffate.di.modules

import android.content.Context
import com.appsdreamers.domain.executor.PostExecutionThread
import com.appsdreamers.domain.executor.ThreadExecutor
import com.iamsourav.supportwheeloffate.base.SupportWheelOfFateApplication
import com.iamsourav.supportwheeloffate.executor.JobExecutor
import com.iamsourav.supportwheeloffate.executor.UIThread
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Sourav Palit on 2019-11-11
 */
@Module
class AppModule(val application : SupportWheelOfFateApplication) {

    @Singleton
    @Provides
    fun provideContext() : Context {
        return application
    }

    @Provides
    @Singleton
    fun provideThreadExecutor(): ThreadExecutor {
        return JobExecutor()
    }

    @Provides
    @Singleton
    fun providePostExecutionThread(): PostExecutionThread {
        return UIThread()
    }
}
package com.iamsourav.supportwheeloffate.di.components

import com.iamsourav.supportwheeloffate.di.modules.AppModule
import com.iamsourav.supportwheeloffate.di.modules.DataModule
import com.iamsourav.supportwheeloffate.di.modules.NetworkModule
import com.iamsourav.supportwheeloffate.di.modules.PresenterModule
import com.iamsourav.supportwheeloffate.ui.engineerlist.view.EngineerListFragment
import com.iamsourav.supportwheeloffate.ui.schedulelist.view.ScheduleListFragment
import dagger.Component
import javax.inject.Singleton

/**
 * Created by Sourav Palit on 2019-11-11
 */
@Singleton
@Component(modules = arrayOf(AppModule::class, NetworkModule::class, DataModule::class, PresenterModule::class))
interface AppComponent {

    fun inject(target: EngineerListFragment)
    fun inject(target: ScheduleListFragment)
}
package com.iamsourav.supportwheeloffate.di.scopes

import javax.inject.Qualifier
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy

/**
 * Created by Sourav Palit on 2019-11-11
 */
@Qualifier
@Retention(RetentionPolicy.RUNTIME)
annotation class UseCase

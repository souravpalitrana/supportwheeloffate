package com.iamsourav.supportwheeloffate.di.modules

import com.appsdreamers.domain.executor.PostExecutionThread
import com.appsdreamers.domain.executor.ThreadExecutor
import com.iamsourav.domain.repositories.EngineerRepository
import com.iamsourav.domain.repositories.ScheduleRepository
import com.iamsourav.domain.usecases.GetEngineerListUseCase
import com.iamsourav.domain.usecases.GetScheduleListUseCase
import com.iamsourav.supportwheeloffate.di.scopes.UseCase
import dagger.Module
import dagger.Provides

/**
 * Created by Sourav Palit on 2019-11-11
 */
@Module
@UseCase
class UseCaseModule {

    @Provides
    fun providesGetEngineerListUseCase(backgroundTaskExecutor: ThreadExecutor,
                              postExecutionThread: PostExecutionThread,
                              repository : EngineerRepository) : GetEngineerListUseCase {
        return GetEngineerListUseCase(backgroundTaskExecutor, postExecutionThread, repository)
    }

    @Provides
    fun providesGetScheduleListUseCase(backgroundTaskExecutor: ThreadExecutor,
                                       postExecutionThread: PostExecutionThread,
                                       repository : ScheduleRepository) : GetScheduleListUseCase {
        return GetScheduleListUseCase(backgroundTaskExecutor, postExecutionThread, repository)
    }
}

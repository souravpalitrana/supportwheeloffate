package com.iamsourav.supportwheeloffate.di.modules

import com.iamsourav.domain.usecases.GetEngineerListUseCase
import com.iamsourav.domain.usecases.GetScheduleListUseCase
import com.iamsourav.supportwheeloffate.di.scopes.PerActivity
import com.iamsourav.supportwheeloffate.ui.engineerlist.EngineerListContract
import com.iamsourav.supportwheeloffate.ui.engineerlist.presenter.EngineerListPresenterImpl
import com.iamsourav.supportwheeloffate.ui.schedulelist.ScheduleListContract
import com.iamsourav.supportwheeloffate.ui.schedulelist.presenter.ScheduleListPresenterImpl
import dagger.Module
import dagger.Provides

/**
 * Created by Sourav Palit on 2019-11-11
 */
@Module
@PerActivity
class PresenterModule {

    @Provides
    internal fun providesEngineerListPresenter(
        getEngineerListUseCase: GetEngineerListUseCase
    ): EngineerListContract.EngineerListPresenter {
        return EngineerListPresenterImpl(getEngineerListUseCase)
    }

    @Provides
    internal fun providesScheduleListPresenter(
        getScheduleListUseCase: GetScheduleListUseCase
    ): ScheduleListContract.ScheduleListPresenter {
        return ScheduleListPresenterImpl(getScheduleListUseCase)
    }
}
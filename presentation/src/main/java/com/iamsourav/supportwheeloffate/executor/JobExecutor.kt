package com.iamsourav.supportwheeloffate.executor

import androidx.annotation.NonNull
import com.appsdreamers.domain.executor.ThreadExecutor
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.ThreadFactory
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit


/**
 * Created by Sourav Palit on 2019-11-11
 */
class JobExecutor : ThreadExecutor {
    internal val INITIAL_POOL_SIZE = 3
    internal val MAX_POOL_SIZE = 5
    internal val KEEP_ALIVE_TIME = 10
    internal val KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS

    var threadPoolExecutor: ThreadPoolExecutor


    init {
        val workQueue = LinkedBlockingQueue<Runnable>()
        this.threadPoolExecutor = ThreadPoolExecutor(
            INITIAL_POOL_SIZE, MAX_POOL_SIZE,
            KEEP_ALIVE_TIME.toLong(), KEEP_ALIVE_TIME_UNIT, workQueue, JobThreadFactory()
        )
    }

    override fun execute(@NonNull runnable: Runnable) {
        this.threadPoolExecutor.execute(runnable)
    }

    internal class JobThreadFactory : ThreadFactory {
        private var counter = 0

        override fun newThread(@NonNull runnable: Runnable): Thread {
            return Thread(runnable, THREAD_NAME + counter++)
        }

        companion object {
            val THREAD_NAME = "android_"
        }
    }
}
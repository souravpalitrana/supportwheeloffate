package com.iamsourav.supportwheeloffate.executor

import com.appsdreamers.domain.executor.PostExecutionThread
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers


/**
 * Created by Sourav Palit on 2019-11-11
 */
class UIThread : PostExecutionThread {

    override val scheduler: Scheduler
        get() = AndroidSchedulers.mainThread()

}
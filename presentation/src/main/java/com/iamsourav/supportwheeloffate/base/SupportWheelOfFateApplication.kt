package com.iamsourav.supportwheeloffate.base

import android.app.Application
import com.iamsourav.supportwheeloffate.di.components.AppComponent
import com.iamsourav.supportwheeloffate.di.components.DaggerAppComponent
import com.iamsourav.supportwheeloffate.di.modules.AppModule

/**
 * Created by Sourav Palit on 2019-11-11
 */
class SupportWheelOfFateApplication : Application() {

    lateinit var appComponent : AppComponent

    companion object {
        lateinit var instance : SupportWheelOfFateApplication
    }

    override fun onCreate() {
        super.onCreate()
        instance = this;
        initDependencyInjection(this)
    }

    private fun initDependencyInjection(application: SupportWheelOfFateApplication) {
        appComponent = DaggerAppComponent.builder().appModule(AppModule(application)).build();
    }

}
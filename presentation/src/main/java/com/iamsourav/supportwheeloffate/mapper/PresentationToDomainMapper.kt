package com.iamsourav.supportwheeloffate.mapper

import com.iamsourav.supportwheeloffate.ui.engineerlist.model.Engineer as PresentationEngineer
import com.iamsourav.domain.entities.Engineer as DomainEngineer

/**
 * Created by Sourav Palit on 2019-11-12
 */

fun mapPresentationToDomainEngineer(presentationnEngineers : List<PresentationEngineer>) : List<DomainEngineer> {
    var domainEngineers = ArrayList<DomainEngineer>()

    for(sourceEngineer in presentationnEngineers) {
        val domainEngineer = DomainEngineer(id = sourceEngineer.id, name = sourceEngineer.name)
        domainEngineers.add(domainEngineer)
    }

    return domainEngineers
}

fun mapDomainToPresentationEngineer(domainEngineers : List<DomainEngineer>) : List<PresentationEngineer> {
    var presentationnEngineers = ArrayList<PresentationEngineer>()

    for(domainEngineer in domainEngineers) {
        val domainEngineer = PresentationEngineer(id = domainEngineer.id, name = domainEngineer.name)
        presentationnEngineers.add(domainEngineer)
    }

    return presentationnEngineers
}

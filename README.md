# SupportWheelOfFate

## Background

  All engineers in Company X take it in turns to support the business for half a day at a time. Currently, generating a schedule that shows who’s turn is it to support the business is being done manually and we need to automate that!
## Tech
 Task is to design and build an online `Support Wheel of Fate`. This should repeat selecting two engineers at random to both complete a half day of support (shift) each to ultimately generate a schedule that shows whose turn is it to support the business.

## Assumptions
- We can assume that Company X has 10 engineers.
- We can assume that the schedule will span two weeks and start on the first working day of the upcoming week.

## Assumptions
Currently there are 4 rules:
 •    There are only two support shifts per day, a day shift and a night shift. 
    •    An engineer can do at most one shift in a day. 
    •    An engineer cannot have more than one shift on any consecutive days. 
    •    Each engineer should have completed 2 shifts of support in any 2 week period. 

`*** These rules are liable to change in the future, so make sure your design is flexible and scalable enough to be able to add new rules.
`
# Solution
* We useed **Clean Code Architecture** to separate our presentation logic from business logic and our data manipulation is happening inside domain layer. As all these three modules is working as separate module in the code base we can change in any module without affecting other.
* To generate the schedule based on rule we used filter pattern here. Using filter pattern gives us super control on our selection. So when a new constraint introduce we will ride a separate rule for that without effecting the overal business logic.
* To show the different types of shcedule our schedule list can show multi level of shifts and engineers.

# Engineer List
![Alt_Text](arts/engineer_list.png)
Coming from Apiary Mock API. However we added basic network errow handling here so that you can retry

# Schedule List
![Alt_Text](arts/2ShiftPerDay1EngPerShift.png) 
![Alt_Text](arts/2ShiftPerDay2EngPerShift.png)
![Alt_Text](arts/3ShiftPerDay1EngPerShift.png)
After clicking generate schedule button this screen will appear and will show the generated schedule.
It will show error if you give an invalid condition
![Alt_Text](arts/FailContraints.png)

## How this app works?
Will update soon :)






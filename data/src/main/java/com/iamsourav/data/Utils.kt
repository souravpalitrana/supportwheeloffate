package com.iamsourav.data

fun isEmpty(str: CharSequence?) : Boolean {
    return str == null || str.length == 0
}
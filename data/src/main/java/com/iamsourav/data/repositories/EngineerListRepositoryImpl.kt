package com.iamsourav.data.repositories

import com.iamsourav.data.mapper.mapDataToDomainEngineer
import com.iamsourav.data.repositories.datasource.apiservices.WheelApiService
import com.iamsourav.domain.entities.Engineer
import com.iamsourav.domain.exceptions.WheelApiError
import com.iamsourav.domain.repositories.EngineerRepository
import io.reactivex.Single
import io.reactivex.rxkotlin.subscribeBy
import java.util.ArrayList

/**
 * Created by Sourav Palit on 2019-11-11
 */
class EngineerListRepositoryImpl(val apiService: WheelApiService) : EngineerRepository {

    override fun getEngineerList(): Single<List<Engineer>> {
        return Single.create { emitter->

            apiService.getEngineerList()
                .subscribeBy(
                    onSuccess = { emitter.onSuccess(mapDataToDomainEngineer(it.engineers)) },
                    onError = { emitter.onError(it)}
                )
        }
    }
}
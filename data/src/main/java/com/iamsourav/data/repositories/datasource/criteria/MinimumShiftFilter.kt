package com.iamsourav.data.repositories.datasource.criteria

import com.iamsourav.domain.base.ScheduleConfig
import com.iamsourav.domain.entities.Engineer
import com.iamsourav.domain.repositories.ShiftRepository
import com.iamsourav.domain.repositories.criteria.FilterCriteria

class MinimumShiftFilter(val shiftRepository: ShiftRepository,
                         val scheduleConfig: ScheduleConfig) : FilterCriteria {

    override fun meetCriteria(date: String,
                              engineerList: MutableList<Engineer>): MutableList<Engineer> {

        val filteredEngineer:MutableList<Engineer> = ArrayList<Engineer>()

        for (engineer in engineerList) {
            if (shiftRepository.getTotalShiftOfEngineer(engineer.id) < scheduleConfig.maxShiftByAnEnginner) {
                filteredEngineer.add(engineer)
            }
        }

        return filteredEngineer
    }
}
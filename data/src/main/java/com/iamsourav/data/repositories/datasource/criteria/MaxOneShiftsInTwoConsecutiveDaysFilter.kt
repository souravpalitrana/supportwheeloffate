package com.iamsourav.data.repositories.datasource.criteria

import android.text.TextUtils
import com.iamsourav.data.isEmpty
import com.iamsourav.domain.base.ScheduleConfig
import com.iamsourav.domain.entities.Engineer
import com.iamsourav.domain.repositories.ShiftRepository
import com.iamsourav.domain.repositories.criteria.FilterCriteria

class MaxOneShiftsInTwoConsecutiveDaysFilter(val shiftRepository: ShiftRepository,
                                             val scheduleConfig: ScheduleConfig) : FilterCriteria {

    override fun meetCriteria(date: String,
                              engineerList: MutableList<Engineer>): MutableList<Engineer> {

        val filteredEngineer = ArrayList<Engineer>()

        for (engineer in engineerList) {
            if (shiftRepository.getTotalShiftOfEngineer(engineer.id) === 0) {
                filteredEngineer.add(engineer)
            } else if (!isWorkedInPreviousDay(date, shiftRepository.getLastShiftTimeOfEngineer(engineer.id))) {
                filteredEngineer.add(engineer)
            }
        }

        return filteredEngineer
    }

    private fun isWorkedInPreviousDay(currentDate: String, workingDate: String): Boolean {
        return if (isEmpty(workingDate) ||
            Math.abs(Integer.valueOf(currentDate) - Integer.valueOf(workingDate)) > 1) {
            false
        } else {
            true
        }
    }
}
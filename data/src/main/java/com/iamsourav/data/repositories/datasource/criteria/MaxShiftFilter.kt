package com.iamsourav.data.repositories.datasource.criteria

import com.iamsourav.domain.base.ScheduleConfig
import com.iamsourav.domain.entities.Engineer
import com.iamsourav.domain.entities.Shift
import com.iamsourav.domain.repositories.ShiftRepository
import com.iamsourav.domain.repositories.criteria.FilterCriteria
/**
 * Created by Sourav Palit on 2019-11-12
 */
class MaxShiftFilter(val shiftRepository: ShiftRepository,
                     val scheduleConfig: ScheduleConfig) : FilterCriteria {

    override fun meetCriteria(date: String,
                              engineerList: MutableList<Engineer>): MutableList<Engineer> {

        val engineerMap = HashMap<Int, MutableList<Engineer>>()

        for (engineer in engineerList) {
            val shiftCount = shiftRepository.getTotalShiftOfEngineer(engineer.id)

            if (engineerMap.containsKey(shiftCount)) {
                 if (engineerMap.get(shiftCount) == null) {
                     val list = ArrayList<Engineer>()
                     list.add(engineer)
                     engineerMap.put(shiftCount, list)
                 } else {
                     engineerMap.get(shiftCount)!!.add(engineer)
                 }
            } else {
                val list:MutableList<Engineer> = ArrayList<Engineer>()
                list.add(engineer)
                engineerMap.put(shiftCount, list)
            }
        }

        val sortedEngineerList = ArrayList<Engineer>()

        for (i in 0..scheduleConfig.maxShiftByAnEnginner) {
            if (engineerMap.contains(i)) {
                sortedEngineerList.addAll(engineerMap.get(i)!!)
            }
        }

        return sortedEngineerList
    }

}
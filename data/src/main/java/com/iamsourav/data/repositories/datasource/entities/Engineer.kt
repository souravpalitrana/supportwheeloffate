package com.iamsourav.data.repositories.datasource.entities

/**
 * Created by Sourav Palit on 2019-11-11
 */
data class Engineer(var id: Int,
                    var name: String) {

}
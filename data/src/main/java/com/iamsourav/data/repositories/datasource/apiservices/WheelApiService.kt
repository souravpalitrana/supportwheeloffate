package com.iamsourav.data.repositories.datasource.apiservices

import com.iamsourav.data.repositories.datasource.entities.EngineerListResponse
import com.iamsourav.domain.entities.Engineer
import io.reactivex.Single
import retrofit2.http.GET

/**
 * Created by Sourav Palit on 2019-11-11
 */
interface WheelApiService {

    @GET("/engineers")
    fun getEngineerList(): Single<EngineerListResponse>
}
package com.iamsourav.data.repositories

import com.iamsourav.domain.entities.Shift
import com.iamsourav.domain.repositories.ShiftRepository

/**
 * Created by Sourav Palit on 2019-11-12
 */
class ShiftRepositoryImpl() : ShiftRepository {

    // A engineer may have multiple shit and also shift has multiple type
    // <EngineerId, <ShiftType, <ShiftList>>
    var shiftMapByEngineer = HashMap<Int, MutableMap<Int, MutableList<Shift>>>()

    override fun getLastShiftTimeOfEngineer(engineerId: Int): String {
        if (shiftMapByEngineer.containsKey(engineerId)) {
            return "${getLastShift(shiftMapByEngineer.get(engineerId)!!)}"
        } else {
            return ""
        }
    }

    override fun getTotalShiftOfEngineer(engineerId: Int): Int {
        if (shiftMapByEngineer.containsKey(engineerId)) {
            return getTotalShift(shiftMapByEngineer.get(engineerId)!!)
        } else {
            return 0
        }
    }

    override fun getShiftCountOfShiftOfEngineer(engineerId: Int, shiftType: Int): Int {
        if (shiftMapByEngineer.containsKey(engineerId)) {
            return getShiftCountByShift(shiftType, shiftMapByEngineer.get(engineerId)!!)
        } else {
            return 0
        }
    }

    override fun addShift(shift: Shift) {
        // check map contains engineer record. If has record check engineer has worked on that shift or not
        // If worked on that type of sheet put there other wise create new
        // Need to do that for all engineer of that shift

        for (engineer in shift.engineers) {
            if (shiftMapByEngineer.containsKey(engineer.id)) {
                addShiftToMap(shift, shiftMapByEngineer.get(engineer.id)!!)
            } else {
                val shiftMap: MutableMap<Int, MutableList<Shift>> = HashMap<Int, MutableList<Shift>>()
                val shifts: MutableList<Shift> = ArrayList<Shift>()
                shifts.add(shift)
                shiftMap.put(shift.type, shifts)
                shiftMapByEngineer.put(engineer.id, shiftMap)
            }
        }
    }

    override fun reset() {
        shiftMapByEngineer = HashMap<Int, MutableMap<Int, MutableList<Shift>>>()
    }

    private fun addShiftToMap(shift: Shift, map: MutableMap<Int, MutableList<Shift>>) {
        // Check engineer worked on that type of shift or not
        if (map.containsKey(shift.type)) {
            map.get(shift.type)!!.add(shift)
        } else {
            val list = ArrayList<Shift>()
            list.add(shift)
            map.put(shift.type, list)
        }
    }

    private fun getLastShift(map: MutableMap<Int, MutableList<Shift>>) : String {
        var dateIndex = 0

        for((key, value) in map) {
            for (shift in value) {
                val shiftDate = Integer.valueOf(shift.date)
                if (shiftDate > dateIndex) {
                    dateIndex = shiftDate
                }
            }
        }

        return "${dateIndex}"
    }

    private fun getTotalShift(map: MutableMap<Int, MutableList<Shift>>) : Int {
        var shiftCount = 0

        for ((key, value) in map) {
            shiftCount = shiftCount + value.size
        }

        return shiftCount
    }

    private fun getShiftCountByShift(shiftType: Int, map: MutableMap<Int, MutableList<Shift>>) : Int {
        if (map.containsKey(shiftType)) {
            return map.get(shiftType)!!.size
        } else {
            return 0
        }
    }
}
package com.iamsourav.data.repositories

import com.iamsourav.domain.base.ScheduleConfig
import com.iamsourav.domain.entities.Engineer
import com.iamsourav.domain.entities.Schedule
import com.iamsourav.domain.entities.Shift
import com.iamsourav.domain.exceptions.WheelInvalidConstraintError
import com.iamsourav.domain.repositories.ScheduleRepository
import com.iamsourav.domain.repositories.ShiftRepository
import com.iamsourav.domain.repositories.criteria.FilterCriteria
import io.reactivex.Single
import java.lang.Exception
import java.util.ArrayList

/**
 * Created by Sourav Palit on 2019-11-12
 */
class ScheduleRepositoryImpl(val scheduleConfig: ScheduleConfig,
                             val shiftRepository: ShiftRepository,
                             val filters : ArrayList<FilterCriteria>) : ScheduleRepository {

    override fun getSchedules(engineerList: List<Engineer>): Single<List<Schedule>> {

        return Single.create { emitter->

            try {
                // Considering Week 1 Day 1 as dateIndex 1. We can work with real date.
                var dateIndex = 1

                val scheduleList = ArrayList<Schedule>()

                // To clean previous data so that we can create new schedule
                shiftRepository.reset()

                for (i in 0 until scheduleConfig.totalWeeks) {

                    for (j in 0 until scheduleConfig.workingDaysPerWeek) {
                        val filterEnginner = applyFilter(engineerList, "${dateIndex}");
                        val schedule = getSchedule("${dateIndex}", filterEnginner)
                        scheduleList.add(schedule)
                        dateIndex++
                    }
                }

                emitter.onSuccess(scheduleList)
            } catch (exception : Exception) {
                emitter.onError(WheelInvalidConstraintError(exception))
            }
        }
    }

    /**
     * Will apply predefined rule on the given list and return filter list
     */
    private fun applyFilter(engineerList: List<Engineer>, date: String) :MutableList<Engineer> {

        var newEngineerList: MutableList<Engineer> = ArrayList()
        newEngineerList.addAll(engineerList)

        for (filter in filters) {
            newEngineerList = filter.meetCriteria(date, newEngineerList)
        }

        return newEngineerList
    }

    /**
     * Will return a schedule by picking engineers from the given list
     */
    private fun getSchedule(date: String, engineers: List<Engineer>): Schedule {

        val shifts:MutableList<Shift> = ArrayList()

        var engineerIndex = 0
        for (i in 0 until scheduleConfig.totalShiftsPerDay) {

            val engineerList:MutableList<Engineer> = ArrayList()

            for (j in 0 until scheduleConfig.numberOfEngineerPerShift) {
                engineerList.add(engineers.get(engineerIndex))
                engineerIndex++
            }

            val shift = Shift(date = date,
                type = i,
                engineers = engineerList)

            shifts.add(shift)

            shiftRepository.addShift(shift)
        }

        val schedule = Schedule(date, shifts)

        return schedule
    }
}
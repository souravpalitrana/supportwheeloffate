package com.iamsourav.data.mapper

import com.iamsourav.data.repositories.datasource.entities.Engineer

/**
 * Created by Sourav Palit on 2019-11-12
 * Used to map data from domain layer to data layer
 */

fun mapDataToDomainEngineer(dataEngineers : List<Engineer>) : List<com.iamsourav.domain.entities.Engineer> {
    var domainEngineers = ArrayList<com.iamsourav.domain.entities.Engineer>()

    for(dataEngineer in dataEngineers) {
        val domainEngineer = com.iamsourav.domain.entities.Engineer(id = dataEngineer.id,
            name = dataEngineer.name)
        domainEngineers.add(domainEngineer)
    }

    return domainEngineers
}
package com.iamsourav.data

import com.iamsourav.data.repositories.ScheduleRepositoryImpl
import com.iamsourav.data.repositories.ShiftRepositoryImpl
import com.iamsourav.data.repositories.datasource.criteria.MaxOneShiftsInTwoConsecutiveDaysFilter
import com.iamsourav.data.repositories.datasource.criteria.MaxShiftFilter
import com.iamsourav.data.repositories.datasource.criteria.MinimumShiftFilter
import com.iamsourav.domain.base.ScheduleConfig
import com.iamsourav.domain.entities.Engineer
import com.iamsourav.domain.repositories.ScheduleRepository
import com.iamsourav.domain.repositories.ShiftRepository
import com.iamsourav.domain.repositories.criteria.FilterCriteria
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.Assert.*
import org.mockito.runners.MockitoJUnitRunner
import java.lang.RuntimeException


@RunWith(MockitoJUnitRunner::class)
class ScheduleRepositoryImplTest {

    private lateinit var shiftRepository: ShiftRepository

    @Before
    fun setup() {
        shiftRepository = ShiftRepositoryImpl()
    }

    @Test
    fun testScheduleHappyCase_2Shit1EngPerShift_Pass() {

        val scheduleConfig = ScheduleConfig()
        val filters = getFilters(shiftRepository, scheduleConfig)

        val scheduleRepository: ScheduleRepository = ScheduleRepositoryImpl(
            scheduleConfig,
            shiftRepository,
            filters)

        val scheduleList = scheduleRepository.getSchedules(getEngineerList()).blockingGet()

        val expectedValue = 10 // 10 Schedules

        assertNotNull(scheduleList)

        assertEquals(expectedValue, scheduleList.size)
    }

    @Test(expected = RuntimeException::class)
    fun testScheduleHappyCase_2Shit1EngPerShift_Failed() {

        val scheduleConfig = ScheduleConfig(
            numberOfEngineerPerShift = 1,
            totalWeeks = 2,
            workingDaysPerWeek = 5,
            totalShiftsPerDay= 2,
            numberOfShiftsEngineerCanDoPerDay = 1,
            minimumDayBetweenNextShift = 1,
            maxShiftByAnEnginner = 1)

        val filters = getFilters(shiftRepository, scheduleConfig)

        val scheduleRepository: ScheduleRepository = ScheduleRepositoryImpl(
            scheduleConfig,
            shiftRepository,
            filters)

        val scheduleList = scheduleRepository.getSchedules(getEngineerList()).blockingGet()
    }

    // Rule 2 : Number of Engineer Per Shift 2, Number of Shift 2
    @Test
    fun testScheduleHappyCase_2Shit2EngPerShift_Pass() {

        val scheduleConfig = ScheduleConfig(
            numberOfEngineerPerShift = 2,
            totalWeeks = 1,
            workingDaysPerWeek = 5,
            totalShiftsPerDay= 2,
            numberOfShiftsEngineerCanDoPerDay = 1,
            minimumDayBetweenNextShift = 2,
            maxShiftByAnEnginner = 2)

        val filters = getFilters(shiftRepository, scheduleConfig)

        val scheduleRepository: ScheduleRepository = ScheduleRepositoryImpl(
            scheduleConfig,
            shiftRepository,
            filters)

        val scheduleList = scheduleRepository.getSchedules(getEngineerList()).blockingGet()

        val expectedValue = 5 // 10 Schedules

        assertNotNull(scheduleList)

        assertEquals(expectedValue, scheduleList.size)
    }

    //Rule 3 Number of Engineer per shift 1, Number of Shift 3
    @Test
    fun testScheduleHappyCase_3Shit1EngPerShift_Pass() {

        val scheduleConfig = ScheduleConfig(
            numberOfEngineerPerShift = 1,
            totalWeeks = 1,
            workingDaysPerWeek = 5,
            totalShiftsPerDay= 3,
            numberOfShiftsEngineerCanDoPerDay = 1,
            minimumDayBetweenNextShift = 3,
            maxShiftByAnEnginner = 2)

        val filters = getFilters(shiftRepository, scheduleConfig)

        val scheduleRepository: ScheduleRepository = ScheduleRepositoryImpl(
            scheduleConfig,
            shiftRepository,
            filters)

        val scheduleList = scheduleRepository.getSchedules(getEngineerList()).blockingGet()

        val expectedValue = 5 // 10 Schedules

        assertNotNull(scheduleList)

        assertEquals(expectedValue, scheduleList.size)
    }

    fun getFilters(shiftRepository: ShiftRepository,
                   scheduleConfig: ScheduleConfig
    ) : ArrayList<FilterCriteria> {

        val filters = ArrayList<FilterCriteria>()
        filters.add(MaxShiftFilter(shiftRepository, scheduleConfig))
        filters.add(MinimumShiftFilter(shiftRepository, scheduleConfig))
        filters.add(MaxOneShiftsInTwoConsecutiveDaysFilter(shiftRepository, scheduleConfig))

        return filters
    }

    fun getEngineerList() : List<Engineer> {

        val engineerList = ArrayList<Engineer>()
        for (i in 0..9) {
            engineerList.add(Engineer(i, "Engineer ${i}"))
        }

        return engineerList
    }
}